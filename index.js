const http = require("http");

const server = http.createServer((req, res) => {
  res.write('hai-v1 intinya harus berubah');
  res.end();
});

const PORT = process.env['PORT'] || 80;

server.listen(PORT, () => {
  process.stdout.write('server listen on port ' + PORT);
});
